package main

import (
	"bytes"
	"encoding/gob"
	"fmt"
	"log"
	"net"
)

const protocol  = "tcp"		//安全保障的网络协议
const nodeVersion  = 1		//版本
const commandlength = 12	//命令行长度

var nodeAddress string		//节点地址
var miningAddress string	//挖矿地址
var knowNodes = []string{"localhost:3000"}		//已经知道的节点
var blockInTransit = [][]byte{}
var mempool=make(map[string]Transaction)		//内存池

type addr struct {
	Addrlist []string	//节点
}
type block struct {
	AddrFrom string		//来源地址
	Block []byte		//块
}

type getblocks struct {
	AddrFrom string
}
type getdata struct {
	AddrFrom string	//来源地址
	Type string	//类型
	ID	[]byte
}
type inv struct {
	AddrFrom string 	//来源
	Type string
	Items [][]byte
}
type tx struct {
	AddrFrom string 	//来源
	Transaction []byte
}
type Version struct {
	Version int		//版本参数
	BestHeight int
	AddrFrom string
}
//字节到命令
func bytesToCommand(bytes []byte)string  {
	var command []byte
	for _,b := range bytes{
		if b!=0x0{
			command = append(command,b)		//增加命令的字节
		}
	}
	return fmt.Sprintf("%s",command)
}
//命令到字节
func commandToBytes(command string) []byte {
	var bytes[commandlength] byte
	for index,char := range command{
		bytes[index] = byte(char)	//字节转化为索引
	}
	return bytes[:]
}
//提取命令
func extractCommand(request []byte)[]byte {
	return request[:commandlength]
}
//请求块
func requestBlocks()  {
	for _,node := range knowNodes{
		sendGetBlock(node)
	}
}
//发送请求块
func sendGetBlock(address string)  {

}
//发送地址
func sendaddr(address string)  {
	
}
//发送块
func sendBlock(addr string,bc *Block)  {
	
}
//发送数据
func sendData(addr string,data []byte)  {
	
}
//发送请求
func snedInv(address,kind string,item [][]byte)  {
	
}
//发送请求多个模块
func sendGetBlocks(address string)  {
	
}
//发送请求的数据
func sendGetData(address,kind string,id []byte)  {
	
}
//发送一个交易
func sendTx(addr string,tnx *Transaction)  {
	
}
//发送版本信息
func sendVersion(addr string,bc *BlockChain)  {
	
}
//模块的句柄
func handleBlock(request []byte,bc *BlockChain)  {
	
}
//请求的版本
func handleInv(request []byte,bc *BlockChain)  {
	
}
//抓取多个模块
func handleGetBlocks(request []byte,bc *BlockChain)  {
	
}
//抓取数据
func handleGetData(request []byte,bc *BlockChain)  {

}
//抓取交易
func handleGetTx(request []byte,bc *BlockChain)  {
	var buff bytes.Buffer
	//var payload tx
	buff.Write(request[commandlength:])
}
//处理版本
func handleVersion(request []byte,bc *BlockChain)  {
	
}
//处理网络连接
func handleConnection(conn net.Conn,bc *BlockChain)  {
	
}
//开启服务器
func StartServer(nodeID,minerAddress string)  {

}
func gobEncode(data interface{})[]byte  {
	var buff bytes.Buffer
	enc := gob.NewEncoder(&buff)	//编码器
	err := enc.Encode(data)		//编码
	if err!=nil {
		log.Panic(err)
	}
	return buff.Bytes()		//字节
}
func nodeIsKnow(addr string)bool  {	//判断一个节点是不是已经知道的节点
	for _,node := range knowNodes{
		if node == addr {
			return true
		}
	}
	return false
}
