package main

import (
	"crypto/sha256"
	"fmt"
	"strconv"
	"time"
)

// 挖矿原理
// zhangyafeiqq1271570224
// sha256（zhangyafeiqq1271570224）= 2ee238957dd8d005cec9703395cd43ab197b485c38d9cb7854e1c0628fc4c33f
// sha256（zhangyafeiqq1271570224）= 2ee238957dd8d005cec9703395cd43ab197b485c38d9cb7854e1c0628f000000

func mainx() {
	start := time.Now()
	for i := 0; i < 1000000; i++ {
		data := sha256.Sum256([]byte(strconv.Itoa(i)))
		fmt.Printf("%10d,%x\n", i, data)
		data1 := string(data[len(data)-2:])
		fmt.Printf("%s\n", data1)
		if data1 == "00"{
			usedtime := time.Since(start)
			fmt.Printf("挖矿成功%d Ms", usedtime)
			break
		}
	}
}