package main

import (
	"fmt"
)

func main() {
	fmt.Println("hello game start")
	bc := NewBlockchain() //创建区块链
	bc.AddBlock("张亚飞1 pay 小花 10")
	bc.AddBlock("张亚飞2 pay 小花 20")
	bc.AddBlock("张亚飞3 pay 小花 30")
	for _, block := range bc.blocks {
		fmt.Printf("上一块哈希%x\n", block.PrevBlockHash)
		fmt.Printf("数据：%s\n", block.Data)
		fmt.Printf("当前哈希%x\n", block.Hash)
		fmt.Println()
	}
}
