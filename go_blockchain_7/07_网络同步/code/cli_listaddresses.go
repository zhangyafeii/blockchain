package main

import (
	"fmt"
	"log"
)

//提取所有钱包的地址
func (cli *CLI)listAddresses(nodeID string)  {
	wallets,err := NewWallets(nodeID)
	if err!=nil{
		log.Panic(err)
	}
	addresses := wallets.GetAddresses()
	for _,addr := range addresses{
		fmt.Println(addr)
	}
}
