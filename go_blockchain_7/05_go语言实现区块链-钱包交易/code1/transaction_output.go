package main

import (
	"bytes"
)

//输出
type TXOutput struct {
	Value int	//output保存了“币”（上面额value）
	PubKeyHash []byte		//用脚本语言意味着比特币可以也作为智能合约平台
}
//输出锁住的标志
func (out *TXOutput)Lock(address []byte)  {
	pubkeyhash := Base58Decode(address)
	pubkeyhash=pubkeyhash[1:len(pubkeyhash)-4]		//截取有效哈希
	out.PubKeyHash = pubkeyhash		//锁住，无法再被修改
}
//检测是否被key锁住
func (out *TXOutput)isLockedWithKey(pubKeyHash []byte) bool {
	return bytes.Compare(out.PubKeyHash,pubKeyHash) == 0
}

//创造一个输出
func NewTXOutput(value int,address string)*TXOutput{
	txo := &TXOutput{Value:value,PubKeyHash:nil}
	txo.Lock([]byte(address))		//锁住
	return txo
}

//是否可以解锁输出
//func (out *TXOutput)CanBeUnlockedWith(unlockingData []byte)bool {
//	return out.ScriptPubkey==unlockingData
//	//return out.PubKeyHash == unlockingData
//}

